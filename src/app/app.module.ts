import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {AppRoutingModule} from './app-routing/app-routing.module';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {LoginComponent} from './components/login/login.component';
import {AlertComponent} from './components/alert/alert.component';
import {HomeComponent} from './components/home/home.component';
import {FormsModule} from '@angular/forms';
import {EmployeeService} from './services/employee/employee.service';
import {AlertService} from './services/alert/alert.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ApplicationInterceptor} from './services/interceptor/application.interceptor';
import {AuthService} from './services/auth/auth.service';
import {AuthGuard} from './services/auth/auth-guard';
import {SummaryComponent} from './components/summary/summary.component';
import {ErrorService} from './services/error/error.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    FooterComponent,
    PageNotFoundComponent,
    AlertComponent,
    HomeComponent,
    SummaryComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    ApplicationInterceptor, AuthService, AuthGuard, AlertService, EmployeeService, ErrorService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApplicationInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
