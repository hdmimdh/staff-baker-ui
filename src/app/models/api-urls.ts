export class ApiUrls {

  public static get LOGIN_URL(): string { return '/login'; }
  public static get EMPLOYEE_URL(): string { return '/employee'; }
}
