export class Employee {

  id: number;
  email: string;
  firstName: string;
  lastName: string;
  department: string;
  gender: string;
  phone: string;
  skype: string;
  telegram: string;
  workingPlace: string;
  title: string;
  pictureUrl: string;
  summary: string;
  birthday: string;
  active: boolean;
  deactivatedAt: string;
  yearsInCompany: number;
  projectsNumber: number;
}
