import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Employee} from '../../models/employee';
import {ApiUrls} from '../../models/api-urls';

@Injectable()
export class EmployeeService {

  constructor(private http: HttpClient) {
  }

  getById(id: number): Observable<Employee> {
    return this.http.get<Employee>(ApiUrls.EMPLOYEE_URL + `/${id}`);
  }

  getAll(): Observable<Employee[]> {
    return this.http.get<Employee[]>(ApiUrls.EMPLOYEE_URL);
  }

  updateSummary(id: number, summary: string): Observable<any> {

    if (!summary) {
      return;
    }

    const url = ApiUrls.EMPLOYEE_URL + `/${id}` + '/summary';
    return this.http.put(url, JSON.stringify({summary: summary}), {responseType: 'text'});
  }
}
