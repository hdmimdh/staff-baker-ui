import {Injectable} from '@angular/core';
import {AlertService} from '../alert/alert.service';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable()
export class ErrorService {

  constructor(private alertService: AlertService) {
  }

  handle(error: HttpErrorResponse) {

    if (!error) {
      return;
    }

    if (error.status === 401 && error.url.indexOf('login') !== -1) {
      this.alertService.error('Your username or password was incorrect');
      return;
    }

    if (error.status === 0) {
      this.alertService.error('We are performing scheduled maintenance. We should be back online shortly.');
      return;
    }
  }
}
