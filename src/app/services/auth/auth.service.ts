import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Employee} from '../../models/employee';
import {ApiUrls} from '../../models/api-urls';

@Injectable()
export class AuthService {

  private redirectUrl: string;

  constructor(private http: HttpClient) {
  }

  getRedirectUrl(): string {
    return this.redirectUrl;
  }

  login(email: string, password: string): Observable<Employee> {

    return this.http.post<Employee>(ApiUrls.LOGIN_URL, JSON.stringify({email: email, password: password}), {observe: 'response'})
      .map(
        response => {

          const employee = response.body;
          if (employee) {
            localStorage.setItem('loggedEmployee', JSON.stringify(employee));
          }

          const token = response.headers.get('authorization');
          if (token) {
            localStorage.setItem('token', token);
          }

          this.redirectUrl = `${ApiUrls.EMPLOYEE_URL}/${employee.id}`;
          console.log(employee);
          return employee;
        });
  }

  logout(): void {
    localStorage.removeItem('loggedEmployee');
    localStorage.removeItem('token');
  }

  token(): string {
    return localStorage.getItem('token');
  }

  loggedEmployee(): Employee {
    return JSON.parse(localStorage.getItem('loggedEmployee'));
  }
}
