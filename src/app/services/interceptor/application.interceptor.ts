import {Injectable, Injector} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {AuthService} from '../auth/auth.service';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';

@Injectable()
export class ApplicationInterceptor implements HttpInterceptor {

  constructor(private inj: Injector) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const auth = this.inj.get(AuthService);

    request = request.clone({
      url: this.url(request.url),
      setHeaders: {
        'Authorization': `${auth.token()}`,
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    });

    return next.handle(request);
  }

  private url(url: string): string {
    url = environment.apiUrl + url;
    return url.replace(/([^:]\/)\/+/g, '$1');
  }
}
