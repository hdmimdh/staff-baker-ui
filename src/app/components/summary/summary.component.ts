import {Component, Input, OnInit} from '@angular/core';
import {EmployeeService} from '../../services/employee/employee.service';
import {AlertService} from '../../services/alert/alert.service';
import {Employee} from '../../models/employee';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {

  @Input() employee: Employee;

  summary: string;

  visibleEdit: boolean;

  constructor(private employeeService: EmployeeService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.summary = this.employee.summary;
  }

  updateSummary() {

    if (this.summary === this.employee.summary) {
      return;
    }

    this.employeeService.updateSummary(this.employee.id, this.summary).subscribe(
      response => {
        this.employee.summary = this.summary;
        this.alertService.success('Employee successfully updated');
      },
      error => {
        this.alertService.error(error.message);
      }
    );
  }

  resetSummary() {
    this.summary = this.employee.summary;
  }

  showEdit() {
    this.visibleEdit = true;
  }

  hideEdit() {
    this.visibleEdit = false;
  }
}
