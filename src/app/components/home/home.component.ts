import {Component, OnInit} from '@angular/core';
import {EmployeeService} from '../../services/employee/employee.service';
import {Employee} from '../../models/employee';
import {ActivatedRoute} from '@angular/router';
import {AlertService} from '../../services/alert/alert.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  employee: Employee;

  constructor(private employeeService: EmployeeService,
              private route: ActivatedRoute,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.loadEmployee(this.route.snapshot.params.id);
  }

  loadEmployee(id: number) {

    if (!id) {
      this.alertService.error('Employee id must be specified');
    }

    this.employeeService.getById(id).subscribe(
      employee => {
        console.log(employee);
        this.employee = employee;
      },
      error => {
        this.alertService.error(error.message);
      }
    );
  }

}
