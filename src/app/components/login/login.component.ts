import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {Router} from '@angular/router';
import {ErrorService} from '../../services/error/error.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any = {};
  loading = false;

  constructor(private router: Router,
              private authService: AuthService,
              private errorService: ErrorService) {
  }

  ngOnInit() {
    this.authService.logout();
  }

  login() {

    this.loading = true;
    this.authService.login(this.model.email, this.model.password)
      .subscribe(
        employee => {
          this.router.navigate([this.authService.getRedirectUrl()]);
        },
        error => {
          this.errorService.handle(error);
          this.loading = false;
        });
  }
}
